package com.example.testcomposaapplication

sealed class AppState {
    object CalendarFeature: AppState()
    data class TimetableFeature(val dayOfMonth: Int): AppState()
}
