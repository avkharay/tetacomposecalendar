package com.example.testcomposaapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.testcomposaapplication.ui.theme.DesignSystemTheme
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.temporal.TemporalAdjusters
import org.threeten.bp.temporal.WeekFields
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : ComponentActivity() {

    private var appState = mutableStateOf<AppState>(AppState.CalendarFeature)
    private var dayOfMonth: Int = 0
    private val featureStack = ArrayDeque<AppState>().apply { push(appState.value) }
    private var calendarZdt = mutableStateOf<ZonedDateTime>(ZonedDateTime.now())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DesignSystemTheme {
                Surface(color = DesignSystemTheme.colors.backgroundPrimary) {
                    renderApplication()

                    //TODO demo
                    //LazyColumnDemo()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (featureStack.size == 1) {
            super.onBackPressed()
        } else {
            featureStack.pop()
            appState.value = featureStack.peek()
        }
    }

    @Composable
    fun renderApplication() {
        // Compose version of context
        val context = LocalContext.current

        val appState = remember { appState }

        val calendarZdt = remember { calendarZdt }

        when (appState.value) {
            is AppState.CalendarFeature -> {
                Column(
                    Modifier
                        .fillMaxSize()
                        .background(DesignSystemTheme.colors.backgroundPrimary)
                ) {
                    val month = SimpleDateFormat("LLLL", Locale.getDefault()).format(
                        calendarZdt.value.toInstant().toEpochMilli()
                    ).uppercase()
                    val year = calendarZdt.value.year
                    Row(
                        modifier = Modifier
                            .padding(20.dp)
                            .fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "$month $year",
                            style = DesignSystemTheme.typography.h3.medium,
                        )
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp), horizontalArrangement = Arrangement.End
                        ) {
                            Image(
                                painter = painterResource(R.drawable.ic_left),
                                contentDescription = null,
                                modifier = Modifier.padding(end = 8.dp).clickable {
                                    this@MainActivity.calendarZdt.value = this@MainActivity.calendarZdt.value.minusMonths(1)
                                }
                            )
                            Image(
                                painter = painterResource(R.drawable.ic_right),
                                contentDescription = null,
                                modifier = Modifier.clickable { this@MainActivity.calendarZdt.value = this@MainActivity.calendarZdt.value.plusMonths(1) }
                            )
                        }

                    }

                    val daysOfWeek = listOf("пн", "вт", "ср", "чт", "пт", "сб", "вс")
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 14.dp),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        daysOfWeek.forEach { dayName ->
                            Text(
                                text = dayName.uppercase(),
                                textAlign = TextAlign.Center,
                                style = DesignSystemTheme.typography.p3.mediumUppercase,
                                color = DesignSystemTheme.colors.textSecondary,
                                modifier = Modifier
                                    .width(50.dp)
                                    .height(16.dp)
                            )
                        }
                    }

                    var zdt = calendarZdt.value.withDayOfMonth(1)
                    val weeksInMonth = calendarZdt.value.with(TemporalAdjusters.lastDayOfMonth())
                        .get(WeekFields.ISO.weekOfMonth())
                    val calendarMonth = calendarZdt.value.month
                    val currentDay = ZonedDateTime.now().dayOfMonth
                    val currentMonth = ZonedDateTime.now().month
                    val currentYear = ZonedDateTime.now().year

                    val clickedDay = remember { mutableStateOf<Int?>(null) }
                    for (i in 0 until weeksInMonth) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 16.dp),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            for (j in 0 until 7) {
                                var text = " "

                                val textColor = if (zdt.dayOfMonth == currentDay && zdt.month == currentMonth && zdt.year == currentYear) {
                                    DesignSystemTheme.colors.brandMtsRed
                                } else if (j in (5..6)) {
                                    DesignSystemTheme.colors.textTertiary
                                } else {
                                    DesignSystemTheme.colors.textPrimary
                                }

                                val textBackground = if (clickedDay.value == zdt.dayOfMonth) {
                                    Color.LightGray
                                } else {
                                    Color.White
                                }

                                var dayOfMonth = 0

                                if (j + 1 == zdt.dayOfWeek.value && calendarMonth == zdt.month) {
                                    text = zdt.dayOfMonth.toString()
                                    dayOfMonth = zdt.dayOfMonth
                                    zdt = zdt.plusDays(1)
                                }

                                var textModifies = Modifier
                                    .height(24.dp)
                                    .width(50.dp)

                                if (text.isNotBlank()) {
                                    textModifies = textModifies
                                        .background(textBackground)
                                        .clickable {
                                            clickedDay.value = dayOfMonth
                                            this@MainActivity.dayOfMonth = dayOfMonth
                                        }
                                }

                                Text(
                                    text = text,
                                    textAlign = TextAlign.Center,
                                    style = DesignSystemTheme.typography.h3.regular,
                                    color = textColor,
                                    modifier = textModifies
                                )
                            }
                        }
                    }

                    //Ни в какую не хочет применяться скругление углов, пытался всеми способами которые смог найти
                    Button(
                        onClick = {
                            this@MainActivity.appState.value = AppState.TimetableFeature(dayOfMonth)
                            featureStack.push(AppState.TimetableFeature(dayOfMonth))
                        },
                        enabled = clickedDay.value != null,
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = DesignSystemTheme.colors.controlsPrimaryActive,
                            disabledBackgroundColor = DesignSystemTheme.colors.controlsInactive
                        ),
                        shape = RoundedCornerShape(6.dp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(40.dp)
                            .background(DesignSystemTheme.colors.controlsPrimaryActive)
                    ) {
                        Text(
                            text = "Открыть расписание",
                            color = Color.White,
                            style = DesignSystemTheme.typography.p2.medium
                        )
                    }
                }
            }
            is AppState.TimetableFeature -> {
                Column {
                    Text(
                        text = "Планы на ${(appState.value as AppState.TimetableFeature).dayOfMonth} число",
                        style = DesignSystemTheme.typography.h3.medium,
                        modifier = Modifier.padding(top = 24.dp, start = 21.dp)
                    )
                    val schedule = mapOf(
                        "00: 00" to "Спать",
                        "01: 00" to "Спать",
                        "02: 00" to "Спать",
                        "03: 00" to "Спать",
                        "04: 00" to "Спать",
                        "05: 00" to "Спать",
                        "05: 01" to "Спать",
                        "05: 02" to "Спать",
                        "05: 03" to "Спать",
                        "05: 04" to "Спать",
                        "06: 00" to "Подъем",
                        "07: 00" to "И так далее список дел",
                        "08: 00" to "Чтобы выходил за пределы экрана"
                    )
                    LazyColumn {
                        items(schedule.toList()) { (timeString, actionString) ->
                            Text(
                                text = "$timeString $actionString",
                                style = DesignSystemTheme.typography.h3.regular,
                                modifier = Modifier
                                    .padding(start = 20.dp, end = 20.dp, top = 60.dp)
                                    .fillMaxWidth()
                            )
                        }
                    }
                }
            }
        }
    }
}